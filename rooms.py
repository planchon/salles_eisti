import requests
from datetime import datetime, timedelta

salles = []

def get_all_rooms():
    base_url = "https://arel.eisti.fr/oauth/token" 

    header_data = {
        "grant_type":"client_credentials",
        "scope":"read",
        "format":"json"
    }

    res = requests.post(base_url, data=header_data, auth=('salles-libres-84', '7egvPwjnC7qnAWiuRfcO'))
    access = res.json()["access_token"]
    print(access)

    header = {
        "Accept": "application/json",
        "Authorization": "bearer " + access,
    }

    res = requests.get("https://arel.eisti.fr/api/campus/rooms?siteId=1990", headers=header)
    room_data = res.json()["rooms"]

    print(room_data)

    def to_string(now):    
        date = now.strftime("%Y-%m-%d")
        heure = now.strftime("%H:%M")
        return date + '%20' + heure

    now = datetime.now()
    start = to_string(now)
    delta = timedelta(minutes=20)
    end = to_string(now + delta)
    
    for room in room_data:
        roomID = room["id"]
        roomLabel = room["label"]
        if 'E' in roomLabel:
            get_salles(header, end, start, roomID, roomLabel)

    return salles

def get_salles(header, end, start, roomID, roomLabel):
    res = requests.get("https://arel.eisti.fr/api/planning/vacantRooms?campusId=1990&end={}&start={}&roomId={}".format(end, start, roomID), headers=header)
    if (len(res.json()["vacantRooms"]) != 0):
        places = res.json()["vacantRooms"][0]["capacity"]
        if places > 29:
            print(res.json())
            salles.append([roomID, roomLabel, places])
            print("la salle {} est libre, il y a {} places".format(roomLabel, places))