from flask import Flask, render_template
from rooms import *

app = Flask(__name__)

@app.route("/")
def hello():
    salles = get_all_rooms()
    return render_template("index.html", salles=salles)

if __name__ == "__main__":
    app.run(debug=True)

